package com.yoprogramo.quizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartActivity extends AppCompatActivity {

    Button btnStartTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        bindViews();
        setOnClickListener();
    }

    private void setOnClickListener() {
        final Intent intent = new Intent(this, MainActivity.class);
        btnStartTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(intent != null){
                    startActivity(intent);
                }
            }
        });
    }

    private void bindViews() {
        btnStartTest = findViewById(R.id.btn_start_test);
    }
}
