package com.yoprogramo.quizapp;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private static final String GRADE = "grade";
    Drawable drawableHappy;
    Drawable drawableSad;
    ImageView imageView;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        bindView();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            displayResult(bundle.getInt(GRADE));
        }
    }

    private void bindView() {
        drawableHappy = getDrawable(R.drawable.ic_sentiment_satisfied_black_24dp);
        drawableSad = getDrawable(R.drawable.ic_sentiment_dissatisfied_black_24dp);
        drawableHappy.setTint(getResources().getColor(R.color.colorAccent));
        drawableSad.setTint(getResources().getColor(R.color.colorAccent));
        imageView = findViewById(R.id.ic_result);
        textView = findViewById(R.id.tv_label);
    }

    private void displayResult(int grade) {
        String result = getString(R.string.passed);
        if (grade >= 8) {
            imageView.setBackground(drawableHappy);
            textView.setText(String.format(getString(R.string.passed),grade));
        } else {
            imageView.setBackground(drawableSad);
            textView.setText(String.format(getString(R.string.fail),grade));
        }
    }

}
