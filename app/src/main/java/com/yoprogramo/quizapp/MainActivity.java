package com.yoprogramo.quizapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String EMPTY_STRING = "";
    private static final String GRADE = "grade";
    private int grade;
    Button btnSubmit, btnReset;
    EditText eTQuestionOneResponse, eTQuestionTwoResponse, eTQuestionThreeResponse;
    RadioGroup rGQuestionFour, rGQuestionFive, rGQuestionSix, getrGQuestionNine, getrGQuestionTen;
    RadioButton rBQuestionFourOption2, rBQuestionFourOption3, rBQuestionFourResponse;
    RadioButton rBQuestionFiveOption1, rBQuestionFiveOption2, rBQuestionFiveResponse;
    RadioButton rBQuestionSixOption1, rBQuestionSixOption2, rBQuestionSixResponse;
    RadioButton rBQuestionNineOption1, rBQuestionNineOption2, rBQuestionNineResponse;
    RadioButton rBQuestionTenOption1, rBQuestionTenOption2, rBQuestionTenResponse;
    CheckBox cBQuestionSevenOption1, cBQuestionSevenOption2, cBQuestionSevenResponse1, cBQuestionSevenResponse2;
    CheckBox cBQuestionEighthOption1, cBQuestionEighthOption2, cBQuestionEighthOption3, cBQuestionEighthResponse1, cBQuestionEighthResponse2;
    TextView tVErrorQuestion4, tVErrorQuestion5, tVErrorQuestion6, tVErrorQuestion7, tVErrorQuestion8, tVErrorQuestion9, tVErrorQuestion10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
        setOnClickEvent();
    }

    private void bindView() {

        eTQuestionOneResponse = findViewById(R.id.et_question1_response);
        eTQuestionTwoResponse = findViewById(R.id.et_question2_response);
        eTQuestionThreeResponse = findViewById(R.id.et_question3_response);

        rGQuestionFour = findViewById(R.id.radio_group_question4);
        rGQuestionFive = findViewById(R.id.radio_group_question5);
        rGQuestionSix = findViewById(R.id.radio_group_question6);
        getrGQuestionNine = findViewById(R.id.radio_group_question9);
        getrGQuestionTen = findViewById(R.id.radio_group_question10);

        rBQuestionFourResponse = findViewById(R.id.question4_response);
        rBQuestionFourOption2 = findViewById(R.id.question4_option2);

        //RadioButtons question4
        tVErrorQuestion4 = findViewById(R.id.tv_error_question4);
        rBQuestionFourOption3 = findViewById(R.id.question4_option2);
        rBQuestionFourOption2 = findViewById(R.id.question4_option3);

        //Question5 views
        tVErrorQuestion5 = findViewById(R.id.tv_error_question5);
        rBQuestionFiveOption1 = findViewById(R.id.question5_option1);
        rBQuestionFiveOption2 = findViewById(R.id.question5_option2);
        rBQuestionFiveResponse = findViewById(R.id.question5_response);

        //Question6 views
        tVErrorQuestion6 = findViewById(R.id.tv_error_question6);
        rBQuestionSixOption1 = findViewById(R.id.question6_option1);
        rBQuestionSixOption2 = findViewById(R.id.question6_option2);
        rBQuestionSixResponse = findViewById(R.id.question6_response);

        //Question7 views
        tVErrorQuestion7 = findViewById(R.id.tv_error_question7);
        cBQuestionSevenOption1 = findViewById(R.id.question7_option1);
        cBQuestionSevenOption2 = findViewById(R.id.question7_option2);
        cBQuestionSevenResponse1 = findViewById(R.id.question7_reponse_1);
        cBQuestionSevenResponse2 = findViewById(R.id.question7_reponse_2);

        //Question8 views
        tVErrorQuestion8 = findViewById(R.id.tv_error_question8);
        cBQuestionEighthOption1 = findViewById(R.id.question8_option1);
        cBQuestionEighthOption2 = findViewById(R.id.question8_option2);
        cBQuestionEighthOption3 = findViewById(R.id.question8_option3);
        cBQuestionEighthResponse1 = findViewById(R.id.question8_reponse_1);
        cBQuestionEighthResponse2 = findViewById(R.id.question8_reponse_2);

        //Question9 views
        tVErrorQuestion9 = findViewById(R.id.tv_error_question9);
        rBQuestionNineOption1 = findViewById(R.id.question9_option1);
        rBQuestionNineOption2 = findViewById(R.id.question9_option2);
        rBQuestionNineResponse = findViewById(R.id.question9_response);

        //Question10 views
        tVErrorQuestion10 = findViewById(R.id.tv_error_question10);
        rBQuestionTenOption1 = findViewById(R.id.question10_option1);
        rBQuestionTenOption2 = findViewById(R.id.question10_option2);
        rBQuestionTenResponse = findViewById(R.id.question10_response);

        //Buttons
        btnSubmit = findViewById(R.id.btn_submit);
        btnReset = findViewById(R.id.btn_reset);

    }

    private void setOnClickEvent() {
        btnSubmit.setOnClickListener(onClickListener);
        btnReset.setOnClickListener(onClickListener);

        rBQuestionFourResponse.setOnClickListener(clicklistener);
        rBQuestionFourOption3.setOnClickListener(clicklistener);
        rBQuestionFourOption2.setOnClickListener(clicklistener);

        rBQuestionFiveOption1.setOnClickListener(clicklistener);
        rBQuestionFiveOption2.setOnClickListener(clicklistener);
        rBQuestionFiveResponse.setOnClickListener(clicklistener);

        rBQuestionFiveOption1.setOnClickListener(clicklistener);
        rBQuestionFiveOption2.setOnClickListener(clicklistener);
        rBQuestionFiveResponse.setOnClickListener(clicklistener);

        rBQuestionSixOption1.setOnClickListener(clicklistener);
        rBQuestionSixOption2.setOnClickListener(clicklistener);
        rBQuestionSixResponse.setOnClickListener(clicklistener);

        cBQuestionSevenOption1.setOnClickListener(checkBoxQuestion7OnClicklistener);
        cBQuestionSevenOption2.setOnClickListener(checkBoxQuestion7OnClicklistener);
        cBQuestionSevenResponse1.setOnClickListener(checkBoxQuestion7OnClicklistener);
        cBQuestionSevenResponse2.setOnClickListener(checkBoxQuestion7OnClicklistener);

        cBQuestionEighthOption1.setOnClickListener(checkBoxQuestion8OnClicklistener);
        cBQuestionEighthOption2.setOnClickListener(checkBoxQuestion8OnClicklistener);
        cBQuestionEighthOption3.setOnClickListener(checkBoxQuestion8OnClicklistener);
        cBQuestionEighthResponse1.setOnClickListener(checkBoxQuestion8OnClicklistener);
        cBQuestionEighthResponse2.setOnClickListener(checkBoxQuestion8OnClicklistener);

        rBQuestionNineOption1.setOnClickListener(clicklistener);
        rBQuestionNineOption2.setOnClickListener(clicklistener);
        rBQuestionNineResponse.setOnClickListener(clicklistener);

        rBQuestionTenOption1.setOnClickListener(clicklistener);
        rBQuestionTenOption2.setOnClickListener(clicklistener);
        rBQuestionTenResponse.setOnClickListener(clicklistener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            final int idView = view.getId();

            switch (idView) {
                case R.id.btn_submit:
                    if (validateInput()) {
                        grade = calculateGrade();
                        Toast.makeText(MainActivity.this, "Submitting", Toast.LENGTH_SHORT).show();
                        Intent intentResultActivity = new Intent(MainActivity.this, ResultActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt(GRADE, grade);
                        intentResultActivity.putExtras(bundle);
                        if (intentResultActivity != null) {
                            startActivity(intentResultActivity);
                        }
                    }
                    break;
                case R.id.btn_reset:
                    resetAswers();
                    Toast.makeText(MainActivity.this, "Resetting", Toast.LENGTH_SHORT).show();
                    break;
            }


        }
    };

    private boolean validateInput() {

        String pendingResponse = getString(R.string.provide_response);

        if (eTQuestionOneResponse.getText().toString().equals(EMPTY_STRING)) {
            eTQuestionOneResponse.setError(pendingResponse);
            eTQuestionOneResponse.requestFocus();
            return false;
        } else if (eTQuestionTwoResponse.getText().toString().equals(EMPTY_STRING)) {
            eTQuestionTwoResponse.setError(pendingResponse);
            eTQuestionTwoResponse.requestFocus();
            return false;
        } else if (eTQuestionThreeResponse.getText().toString().equals(EMPTY_STRING)) {
            eTQuestionThreeResponse.setError(pendingResponse);
            eTQuestionThreeResponse.setContentDescription(getString(R.string.provide_response));
            return false;
        } else if (rGQuestionFour.getCheckedRadioButtonId() == -1) {
            tVErrorQuestion4.setError("");
            tVErrorQuestion4.setHint(pendingResponse);
            return false;
        } else if (rGQuestionFive.getCheckedRadioButtonId() == -1) {
            tVErrorQuestion5.setError("");
            tVErrorQuestion5.setHint(pendingResponse);
            return false;
        } else if (rGQuestionSix.getCheckedRadioButtonId() == -1) {
            tVErrorQuestion6.setError("");
            tVErrorQuestion6.setHint(pendingResponse);
            return false;
        } else if (!cBQuestionSevenOption1.isChecked() &&
                !cBQuestionSevenOption2.isChecked() &&
                !cBQuestionSevenResponse1.isChecked() &&
                !cBQuestionSevenResponse2.isChecked()) {
            tVErrorQuestion7.setError("");
            tVErrorQuestion7.setHint(pendingResponse);
            return false;
        } else if (!cBQuestionEighthOption1.isChecked() &&
                !cBQuestionEighthOption2.isChecked() &&
                !cBQuestionEighthOption3.isChecked() &&
                !cBQuestionEighthResponse1.isChecked() &&
                !cBQuestionEighthResponse2.isChecked()) {
            tVErrorQuestion8.setError("");
            tVErrorQuestion8.setHint(pendingResponse);
            return false;
        } else if (getrGQuestionNine.getCheckedRadioButtonId() == -1) {
            tVErrorQuestion9.setError("");
            tVErrorQuestion9.setHint(pendingResponse);
            return false;
        } else if (getrGQuestionTen.getCheckedRadioButtonId() == -1) {
            tVErrorQuestion10.setError("");
            tVErrorQuestion10.setHint(pendingResponse);
            return false;
        } else {
            return true;
        }

    }


    private int calculateGrade() {
        int gradeQuestion1 = 0, gradeQuestion2 = 0, gradeQuestion3 = 0, gradeQuestion4 = 0, gradeQuestion5 = 0;
        int gradeQuestion6 = 0, gradeQuestion7 = 0, gradeQuestion8 = 0, gradeQuestion9 = 0, gradeQuestion10 = 0;

        gradeQuestion1 = calculateGradeQuestion1();
        gradeQuestion2 = calculateGradeQuestion2();
        gradeQuestion3 = calculateGradeQuestion3();
        gradeQuestion4 = calculateGradeQuestion4();
        gradeQuestion5 = calculateGradeQuestion5();
        gradeQuestion6 = calculateGradeQuestion6();
        gradeQuestion7 = calculateGradeQuestion7();
        gradeQuestion8 = calculateGradeQuestion8();
        gradeQuestion9 = calculateGradeQuestion9();
        gradeQuestion10 = calculateGradeQuestion10();

        return gradeQuestion1 + gradeQuestion2 +
                gradeQuestion3 + gradeQuestion4 + gradeQuestion5 + gradeQuestion6 +
                gradeQuestion7 + gradeQuestion8 + gradeQuestion9 + gradeQuestion10;
    }

    private int calculateGradeQuestion10() {
        if (rBQuestionTenResponse.isChecked()) {
            return 1;
        }
        return 0;
    }

    private int calculateGradeQuestion9() {
        if (rBQuestionNineResponse.isChecked()) {
            return 1;
        }
        return 0;
    }

    private int calculateGradeQuestion8() {

        if (cBQuestionEighthResponse1.isChecked() && cBQuestionEighthResponse2.isChecked()) {
            return 1;
        }
        return 0;
    }

    private int calculateGradeQuestion7() {
        if (cBQuestionSevenResponse1.isChecked() && cBQuestionSevenResponse2.isChecked()) {
            return 1;
        }
        return 0;
    }

    private int calculateGradeQuestion6() {
        if (rBQuestionSixResponse.isChecked()) {
            return 1;
        }
        return 0;
    }

    private int calculateGradeQuestion5() {

        if (rBQuestionFiveResponse.isChecked()) {
            return 1;
        }
        return 0;
    }

    private int calculateGradeQuestion4() {

        if (rBQuestionFourResponse.isChecked()) {
            return 1;
        }
        return 0;
    }

    private int calculateGradeQuestion3() {
        String responseThree = getResources().getString(R.string.reponse_3);
        String reponse = eTQuestionThreeResponse.getText().toString();

        if (responseThree != null) {
            if (reponse.equalsIgnoreCase(responseThree)) {
                return 1;
            }
        }
        return 0;
    }

    private int calculateGradeQuestion2() {
        String responseTwo = getResources().getString(R.string.reponse_2);
        String reponse = eTQuestionTwoResponse.getText().toString();

        if (responseTwo != null) {
            if (reponse.equalsIgnoreCase(responseTwo)) {
                return 1;
            }
        }
        return 0;
    }

    private int calculateGradeQuestion1() {
        String responseOne = getResources().getString(R.string.reponse_1);
        String responseOneAlternative = getResources().getString(R.string.reponse_1_variante);
        String reponse = eTQuestionOneResponse.getText().toString();
        if (responseOne != null && responseOneAlternative != null) {
            if (reponse.equalsIgnoreCase(responseOne) ||
                    reponse.equalsIgnoreCase(responseOneAlternative)) {
                return 1;
            }
        }
        return 0;
    }

    View.OnClickListener checkBoxQuestion7OnClicklistener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            boolean checked = ((CheckBox) view).isChecked();

            switch (view.getId()) {
                case R.id.question7_option1:
                    tVErrorQuestion7.setError(null);
                    tVErrorQuestion7.setHint(null);
                    break;
                case R.id.question7_option2:
                    tVErrorQuestion7.setError(null);
                    tVErrorQuestion7.setHint(null);
                    break;
                case R.id.question7_reponse_1:
                    tVErrorQuestion7.setError(null);
                    tVErrorQuestion7.setHint(null);
                    break;
                case R.id.question7_reponse_2:
                    tVErrorQuestion7.setError(null);
                    tVErrorQuestion7.setHint(null);
                    break;
            }

        }
    };


    View.OnClickListener checkBoxQuestion8OnClicklistener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            boolean checked = ((CheckBox) view).isChecked();

            switch (view.getId()) {
                case R.id.question8_option1:
                    tVErrorQuestion8.setError(null);
                    tVErrorQuestion8.setHint(null);
                    break;
                case R.id.question8_option2:
                    tVErrorQuestion8.setError(null);
                    tVErrorQuestion8.setHint(null);
                    break;
                case R.id.question8_option3:
                    tVErrorQuestion8.setError(null);
                    tVErrorQuestion8.setHint(null);
                    break;
                case R.id.question8_reponse_1:
                    tVErrorQuestion8.setError(null);
                    tVErrorQuestion8.setHint(null);
                    break;
                case R.id.question8_reponse_2:
                    tVErrorQuestion8.setError(null);
                    tVErrorQuestion8.setHint(null);
                    break;
            }

        }
    };

    View.OnClickListener clicklistener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.question4_option2:
                    tVErrorQuestion4.setError(null);
                    tVErrorQuestion4.setHint(null);
                    break;
                case R.id.question4_option3:
                    tVErrorQuestion4.setError(null);
                    tVErrorQuestion4.setHint(null);
                    break;
                case R.id.question4_response:
                    tVErrorQuestion4.setError(null);
                    tVErrorQuestion4.setHint(null);
                    break;
                case R.id.question5_option1:
                    tVErrorQuestion5.setError(null);
                    tVErrorQuestion5.setHint(null);
                    break;
                case R.id.question5_option2:
                    tVErrorQuestion5.setError(null);
                    tVErrorQuestion5.setHint(null);
                    break;
                case R.id.question5_response:
                    tVErrorQuestion5.setError(null);
                    tVErrorQuestion5.setHint(null);
                    break;
                case R.id.question6_option1:
                    tVErrorQuestion6.setError(null);
                    tVErrorQuestion6.setHint(null);
                    break;
                case R.id.question6_option2:
                    tVErrorQuestion6.setError(null);
                    tVErrorQuestion6.setHint(null);
                    break;
                case R.id.question6_response:
                    tVErrorQuestion6.setError(null);
                    tVErrorQuestion6.setHint(null);
                case R.id.question9_option1:
                    tVErrorQuestion9.setError(null);
                    tVErrorQuestion6.setHint(null);
                    break;
                case R.id.question9_option2:
                    tVErrorQuestion9.setError(null);
                    tVErrorQuestion9.setHint(null);
                    break;
                case R.id.question9_response:
                    tVErrorQuestion9.setError(null);
                    tVErrorQuestion9.setHint(null);
                    break;
                case R.id.question10_option1:
                    tVErrorQuestion10.setError(null);
                    tVErrorQuestion10.setHint(null);
                    break;
                case R.id.question10_option2:
                    tVErrorQuestion10.setError(null);
                    tVErrorQuestion10.setHint(null);
                    break;
                case R.id.question10_response:
                    tVErrorQuestion10.setError(null);
                    tVErrorQuestion10.setHint(null);
                    break;
            }

        }
    };


    private void resetAswers() {
        eTQuestionOneResponse.setText(EMPTY_STRING);
        eTQuestionTwoResponse.setText(EMPTY_STRING);
        eTQuestionThreeResponse.setText(EMPTY_STRING);
        rGQuestionFour.clearCheck();
        rGQuestionFive.clearCheck();
        rGQuestionSix.clearCheck();
        getrGQuestionNine.clearCheck();
        getrGQuestionTen.clearCheck();

        cBQuestionSevenOption1.setChecked(false);
        cBQuestionSevenOption2.setChecked(false);
        cBQuestionSevenResponse1.setChecked(false);
        cBQuestionSevenResponse2.setChecked(false);

        cBQuestionEighthOption1.setChecked(false);
        cBQuestionEighthOption2.setChecked(false);
        cBQuestionEighthOption3.setChecked(false);
        cBQuestionEighthResponse1.setChecked(false);
        cBQuestionEighthResponse2.setChecked(false);
    }
}
